<?php
require_once('connect.php');
session_start();
$connection = new mysqli($host , $db_user ,$db_password,$db_name) or die("ERROR");
if(!$connection) {
        echo "ERROR MySQL: Connect to Server\n";
        exit;
}


?>


<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<title>Jaskinia Książek</title>
	<meta name="description" content="Zagłąb się w jakini książęk i odkryj ich historię!" />
	<meta name="keywords" content="książki, gatunki książek, czytanie, jaskinia książek" />
	<link rel="stylesheet" href="logo.css" type="text/css">
	
	
	
	<style>
body  {
  background-image: url("tło2.jpg");
  
}
ul.ukladPoziomy, ul.ukladPoziomy li {
	display: block;
	list-style: none;
	margin: 0;
	padding: 0;
	
}


ul.ukladPoziomy {
	position: absolute;
}

ul.ukladPoziomy li {
	float: left;
	border-width: 0 1px 1px 1px;
	border-style: solid;
	border-color: transparent #25b #25b #9ce;
	margin-left: 120px;
	
}

ul.ukladPoziomy a:link, ul.ukladPoziomy a:visited {
	text-decoration: none;
	display: block;
	width: 142px;
	text-align: center;
	padding: 5px 0;
	font-weight: bold;
	background-color: #27c;
	color: #def;
	border-top: 10px solid #25b;
}

ul.ukladPoziomy a:hover {
	background-color: #28e;
	color: #fff;
	border-top: 20px solid #26d;
	
}

#menuPoziome {
    margin      : 0px;
    border-left : 0px;
    padding     : 0px;
    width       : 1200px;
	height: 40px;
    background-color : blue;
}
	#container
	{
		width: 1200px;
		height: 1500px;
		margin-left: auto;
		margin-right: auto;
		background-color : white;
	}
	
	
	#lewy_pasek
	{
		float: left;
		background-color: lightgray;
		width: 280px;
		min-height: 828px;
		padding: 10px;
	}
	#content
	{
		float: left;
		padding: 20px;
		width: 560px;
		height:800px;
	}
	#prawy_pasek
	{
		float: left;
		width: 280px;
		min-height: 828px;
		padding: 10px;
		background-color: lightgray;
	}
	#footer
	{
		clear: both;
		background-color: black;
		color: white;
		text-align: center;
		padding: 20px;
	}	
	
	#newsletter{
	
	
		clear: both;
		background-color: pink;
		color: white;
		height:180px;
		text-align: center;
		
		padding: 10px;
	
	}
	
	#logowanie{
	
		float: left;
		background-color: white;
		width: 260px;
		min-height: 100px;
		padding: 10px;
		text-align: center;
		margin-left: auto;
		margin-right: auto;
	
	}
	#rejestracja{
	
		float: left;
		background-color: white;
		width: 260px;
		text-align: center;
		min-height: 100px;
		padding: 10px;
		margin-left: auto;
		margin-right: auto;
	
	}
	
	form{
		text-align: center;
		margin-left: auto;
		margin-right: auto;
	}
	
	.button {
    display: block;
    height: 50px;
    width: 280px;
    background: #ff9f3f;
    border: 2px solid rgba(33, 68, 72, 0.59); 
}

#wyszukiwanie{
	text-align: left;
}


	</style>

</head>

<body>

	<div id="container">
		
			<img id="logo_zdjecie" alt="logo_zdjecie" src="zdjęcia/log.jpeg">
		<div id="logo"><div style="position:absolute; bottom:450px; left:400px;font-family: bradley hand itc; font-size:100px; color:white;">Jaskinia Ksiazek</div>
		</div>
		<div id="menuPoziome">
		<ul class="ukladPoziomy">
			<li><a href="">Nowości</a></li>
			<li><a href="">Polecane</a></li>
			<li><a href="">Sekcja cytatów</a></li>
			<li><a href="">Kontakt</a></li>
		</ul>
        </div>
	
	
		<div id="lewy_pasek">
		
		
		<div id="logowanie">
			
			
			
			<?php if (empty($_SESSION['user'])) : ?>
			
			<h3>Logowanie</h3>
			<form action="login.php" method="post">
			<label for="login">Login: </label>
			<input id="login" type="text" name="login" /> 
			<br/><br/> 
			<label for="password">Hasło: </label>
			<input id ="password" type="password" name="password" />
			<br/><br/>
			<button type="submit">Zaloguj się</button>
			
			</form>
			<?php else : ?>
			<p>Witaj, <?=$_SESSION['user']?></p>
			
			
			<form method="post"> <input type="submit" onclick="myFunction()" name="przeczytane" value="Moje Konto"/></form> 
		


			<br/><br/>
			<a href="logout.php">
			
				<button type="submit">Wyloguj sie</button></a><br/><br/>
			<br/><br/>
			<form action="email.php"  method="post"><button type="submit">Zmień email</button>	<br/>
						<label for="nowy_email">nowy email: </label><input id="nowy_email" type="text" name="email" /> </form>	
			<br/><br/>	
			<form action="delete.php"  method="post"><button type="submit">Usuń konto</button></form>
			
			<?php endif; ?>
			
			
			
			</div>
			
		
			<div id="rejestracja">
			
			
			
			<?php if (empty($_SESSION['user'])) : ?>
			<h3>Rejestracja</h3>
			<form action="registration.php" method="post">
			
			<label for="logiin">Login: </label>
			<input id="logiin" type="text" name="login" /> 
			<br/><br/> 
			<label for="passwordd">Hasło: </label>
			<input id="passwordd" type="password" name="password" />
			<br/><br/>
			<label for="email">Email: </label>
			<input id="email" type="email" name="email" />
			<br/><br/> 
			<button type="submit">Zapisz sie</button>
			</form>
			<?php endif; ?>
						
			</div>
		
			
			<div id="kategorie">
			<h2>Kategorie</h2>
			<button class="button">Przygoda</button>
			<button class="button">Fantastyka</button>
			<button class="button">Komedia</button>
			<button class="button">Romans</button>
			<button class="button">Dramat</button>
			<button class="button">Horror</button>
			<button class="button">Biografia</button>
			
			
			
			</div>
			
			
		</div>
		
		<div id="content">
		<form id="wyszukiwanie" method="get" action="<?=$_SERVER['PHP_SELF']?>">Wyszukiwanie:
			<input type="text" name="word" />
			<select id="opcje_wyszukiwania">
				<option>Tytuł</option>
				<option>Autor</option>				
				<option>Kategoria</option>
			</select>
			<input type="submit" onclick="funPobierz()" name="submit" value="Szukaj" />
		</form><br><br/>
		
		
		
		
		<?php
		if (isset($_GET['submit'])){
	
			$keyword = "";
				if(isset($_GET['word'])) {
					$keyword = $_GET['word'];
				}
			$keyword = trim($keyword);
			
				
					
			$sql = "SELECT * FROM książki WHERE tytuł LIKE '%$keyword%' ORDER BY tytuł"; 
			 
			$wynik = $connection->query($sql);
			echo "<table cellpadding=\"2\" border=1>";
			while($r = $wynik->fetch_assoc()) {
				echo "<tr>";
				echo "<td>";
		?>
		<a href="dodaj.php">
			
		<form method="post"> <input type="submit" name="wyswietl_ksiazke" value="Zobacz ksiazke"/></form> 
		
		<?php  "</td>";
        echo "<td>".$r['autor']."</td>";
        echo "<td>".$r['tytuł']."</td>";
		echo "<td>".$r['kategoria']."</td>";
        
        echo "</tr>"; 
    }
    echo "</table>"; 
	
	}
	

	include('konto_uztkownika.php');
?>
	
			<br/><br/> 
		
		
		</div>
		
		<div id="prawy_pasek">
			
		</div>
		
		<div id="newsletter">
			newsletter
		</div>
		
		
		<div id="footer">
			Jakiniaksiążekpl
		</div>
	
	</div>


</body>


</html>