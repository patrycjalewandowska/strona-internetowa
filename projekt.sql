-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 15 Kwi 2021, 14:57
-- Wersja serwera: 10.4.17-MariaDB
-- Wersja PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projekt`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nazwa_admina` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `książki`
--

CREATE TABLE `książki` (
  `id_książki` int(11) NOT NULL,
  `tytuł` varchar(255) NOT NULL,
  `wydawnicwto` varchar(255) NOT NULL,
  `autor` varchar(255) DEFAULT NULL,
  `kategoria` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `książki`
--

INSERT INTO `książki` (`id_książki`, `tytuł`, `wydawnicwto`, `autor`, `kategoria`) VALUES
(3, 'Droga królów 1', 'Mag', 'Brandon Sanderson', 'fantasy'),
(4, 'Droga królów 2', 'Mag', 'Brandon Sanderson', 'fantasy'),
(5, 'Harry Potter i Kamień Filozoficzny', 'Media Rodzina', 'Joanne Kathleen Rowling', 'fantasy'),
(6, 'Romeo i Julia. Hamlet. Makbet', 'Greg', 'William Szekspir', 'dramat'),
(7, 'Instytut', 'Uroboros', 'K. C. Archer', 'fantasy\r\n'),
(8, 'Wiedźmin. Tom 1. Ostatnie życzenie', 'SuperNowa', 'Andrzej Sapkowski', 'fantasy'),
(9, 'Droga stali i nadziei. Uniwersum Metro 2033', 'Insignis', 'Dimitrij Manasypow', 'fantasy'),
(10, 'To', 'Albatros', 'Stephen King', 'horror\r\n'),
(11, 'Lśnienie', 'Prószyński i S-ka', 'Stephen King', 'horror'),
(12, 'Smętarz dla zwierzaków', 'Prószyński Media', '	\r\nKing Stephen', 'horror');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przeczytane`
--

CREATE TABLE `przeczytane` (
  `id_przeczytane` int(11) NOT NULL,
  `id_ksiązka` int(11) NOT NULL,
  `id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przeczytane`
--

INSERT INTO `przeczytane` (`id_przeczytane`, `id_ksiązka`, `id_users`) VALUES
(1, 5, 13),
(2, 9, 13),
(3, 11, 19),
(4, 10, 19);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(1, 'Kowalski', '$2y$10$NIYEBMR7P5VajSFbb3Dy8.uTvAaZqsJ9gdQ06Po3PxhH7W86YX/MG', 'kowalski@kowal.pl'),
(2, 'Admin', '$2y$10$4lGOg3G3XuU49JM52qNBw.Ro2AOLrlpnNSCvzSpvwW2/Z.2IlmJX.', 'admin@pooczta.pl'),
(13, 'jacek', '$2y$10$.ernAmVQfxMyq88UIjpesu0aFgP4KlkTohjlEeKVk8ErgzlqVm47m', 'jacek@gmail.com'),
(19, 'ania123', '$2y$10$3uXyhQdX7gle9mWndk1HgORVMgn597/RvM23wO/539baGYXbfJKWy', 'ania1234@gmail.com');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeksy dla tabeli `książki`
--
ALTER TABLE `książki`
  ADD PRIMARY KEY (`id_książki`);

--
-- Indeksy dla tabeli `przeczytane`
--
ALTER TABLE `przeczytane`
  ADD PRIMARY KEY (`id_przeczytane`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `książki`
--
ALTER TABLE `książki`
  MODIFY `id_książki` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `przeczytane`
--
ALTER TABLE `przeczytane`
  MODIFY `id_przeczytane` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
